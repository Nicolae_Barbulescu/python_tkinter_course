import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from windows import set_dpi_awareness

set_dpi_awareness()

root = tk.Tk()
root.geometry("600x400")
root.resizable(False, False)
root.title("Widget Examples")

image = Image.open("images/logo.jpg").resize((200, 200))
photo = ImageTk.PhotoImage(image)

label = ttk.Label(root,text="Image and text", image=photo, padding=5, compound="right")

#label.config(font=("Segoe UI", 20))
#greeting = tk.StringVar()
#label = ttk.Label(root, padding=10, textvariable=greeting)

label.pack()

#greeting.set("Hello there")

root.mainloop()
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from windows import set_dpi_awareness

set_dpi_awareness()


def get_text_content():
    print(text_content)


root = tk.Tk()
root.geometry("600x400")
root.resizable(False, False)
root.title("Widget Examples")

text = tk.Text(root, height=8)
text.pack()

button = ttk.Frame(root, padding=(20, 10))
button.pack(fill="both")

# Line number.Character Number
text.insert("1.0", "Please enter text...")
text["state"] = "normal"  # disabled

text_content = text.get("1.0", "end")
get_text = ttk.Button(root, text="Send", command=get_text_content)
get_text.pack(side="left")
close_button = ttk.Button(root, text="Close", command=root.destroy)
close_button.pack(side="right")


root.mainloop()
